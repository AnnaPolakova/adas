/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Color;
import java.awt.Graphics;
import static java.lang.Math.min;
import javax.swing.JPanel;

/**
 *
 * @author Anna
 */
public class Arrow extends JPanel {

    private final JPanel obj1;
    private final JPanel obj2;

    public Arrow(JPanel obj1, JPanel obj2) {
        this.obj1 = obj1;
        this.obj2 = obj2;

        setOpaque(false);
        this.updatePosition();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.red);
        g.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
        //g.drawRect(obj1.getWidth() - 1, (obj1.getHeight() - 1) / 2, obj2.getWidth() - obj2.getWidth(), (obj2.getHeight() - 1) / 2);

        g.setColor(Color.BLACK);
        g.drawLine(0, getHeight() - 1, getWidth() - 1, 0);
    }

    public final void updatePosition() {
        setSize(obj2.getX() - obj1.getX(), obj1.getY());
        setLocation(min(obj1.getX(), obj2.getX()), min(obj1.getY(), obj2.getY()));
    }
}
