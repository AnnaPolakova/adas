/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import model.Event;

/**
 *
 * @author Anna
 */
public class EventShape extends ShapeBase {
    private final Event event;

    public EventShape(Event event, Window window) {
        super(window);
        setSize(50, 50);
        this.event = event;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.setColor(Color.green);
        g.fillOval(0, 0, getWidth() - 1, getHeight() - 1);
        g.setColor(Color.black);
        g.drawOval(0, 0, getWidth() - 1, getHeight() - 1);
        
        Point m = getStringSize(g, event.getName());
        g.drawString(event.getName(), (getWidth() - m.x) / 2, (getHeight() + m.y) / 2);
    }
}
