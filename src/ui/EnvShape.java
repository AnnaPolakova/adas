/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import model.Environment;

/**
 *
 * @author Anna
 */
public class EnvShape extends ShapeBase {
    private final Environment env;

    public EnvShape(Environment env, Window window) {
        super(window);
        this.env = env;
        setSize(70, 70);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        final String name = env.getEnvironmentName();
        final Point m = getStringSize(g, name);
        
        final int w = getWidth() - 1;
        final int textPadding = 5;
        final int h = getHeight() - m.y - textPadding - 1;
        final double ratio = .6;
        final int oh = (int)(h * ratio);

        g.setColor(Color.yellow);
        g.fillOval(0, h - oh, w, oh);

        g.setColor(Color.black);
        g.drawOval(0, h - oh, w, oh);
                
        g.setColor(Color.yellow);
        g.fillRect(0, oh / 2, w, h - oh);

        g.setColor(Color.yellow);
        g.fillOval(0, 0, w, oh);

        g.setColor(Color.black);
        g.drawOval(0, 0, w, oh);
        g.drawLine(0, oh / 2, 0, h - oh / 2);
        g.drawLine(w, oh / 2, w, h - oh / 2);

        g.drawString(name, (w - m.x) / 2, getHeight());
    }
}
