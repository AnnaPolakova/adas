/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import model.Actor;
import model.Environment;

/**
 *
 * @author Anna
 */
public class ActorShape extends ShapeBase {
    
    private final Actor actor;
    
    public ActorShape(Actor actor, Window window) {
        super(window);
        this.actor = actor;
        setSize(100, 100);
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.setColor(Color.BLACK);
        g.drawOval((getWidth() - 1) / 2 - 10, 0, 20, 20);
        g.drawLine((getWidth() - 1) / 2, 20, (getWidth() - 1) / 2, 60);
        g.drawLine(20, 30, getWidth() - 1 - 20, 30);
        g.drawLine((getWidth() - 1) / 2, 60, 20, getHeight() - 1);
        g.drawLine((getWidth() - 1) / 2, 60, getWidth() - 1 - 20, getHeight() - 1);
        
    }
    
}
