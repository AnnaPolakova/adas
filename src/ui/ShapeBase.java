/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import javax.swing.JPanel;

/**
 *
 * @author Anna
 */
public abstract class ShapeBase extends JPanel implements MouseListener, MouseMotionListener {

    private Point dragPoint;
    private Point dragLocation;
    protected Window window;

    public ShapeBase(Window window) {
        this.window = window;
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
    }

    protected static Point getStringSize(Graphics g, String str) {
        Graphics2D g2 = (Graphics2D) g;
        FontRenderContext frc = g2.getFontRenderContext();
        GlyphVector gv = g2.getFont().createGlyphVector(frc, str);
        final Rectangle r = gv.getPixelBounds(null, 0, 0);
        return new Point(r.width, r.height);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        dragPoint = e.getLocationOnScreen();
        dragLocation = getLocation();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        final Point p = e.getLocationOnScreen();
        setLocation(dragLocation.x + p.x - dragPoint.x, dragLocation.y + p.y - dragPoint.y);
        window.onObjectMove();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}
