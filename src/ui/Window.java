package ui;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import model.Actor;
import model.Environment;
import model.Event;

public class Window {

    ArrayList<Arrow> arrows;

    public Window() {
        this.arrows = new ArrayList();
        JFrame frame = new JFrame("Vižu modeļa veidošana");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        JPanel canvas = new JPanel();
        frame.setContentPane(canvas);
        canvas.setVisible(true);
        canvas.setBackground(Color.white);
        canvas.setLayout(null);

        final EventShape x = new EventShape(new Event("xxx", "yyy"), this);
        canvas.add(x);
        x.setLocation(20, 30);

        final ShapeBase y = new EnvShape(new Environment("xxx", "yyy", false, false, false, 0, ""),this);
        canvas.add(y);
        y.setLocation(200, 300);

        final ActorShape z = new ActorShape(new Actor("xxx", "yyy"), this);
        canvas.add(z);
        z.setLocation(600, 100);

        final Arrow arr = new Arrow(y, z);
        canvas.add(arr);
        arrows.add(arr);
    }

    public void onObjectMove() {
        for (Arrow a : arrows) {
            a.updatePosition();
        }
    }
}
