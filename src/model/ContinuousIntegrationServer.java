package model;

/*
 Klase realize nepārtrauktas integrācijas serveri PIAM un PSAM modelī. 
 Tieši no nepārtrauktās integrācijas servera tiek pārvaldības visas darbības (Actions),
 kas saistītas ar programmatūras izmaiņu pārnešanu starp vidēm.
 */
public class ContinuousIntegrationServer {

    /*
     Platformas nosaukums kurā funkcionē dotais serveris, piemēram: Linux, Windows, Android utt.
     */
    private String PlatformName;

    /*
     Risinājuma nosaukums vai nepārtrauktās integrācijas servera nosaukums, piemēram: Jenkins, Bamboo, CruiseControl utt.
     */
    private String SolutionName;

    /*
     Nepieciešamu rīku uzskaitījums, kas jāinstalē lai funkcionētu nepārtrauktās integrācijas serveris.
     */
    private String NeededTools;

    /*
     Instalācijas norādījumi
     */
    private String SetupNotes;

    /*
     Gatavu risinājumu atrašanas vietas, norādījumi
     */
    private String LocationsOfSolutions;

    /*
     Atgriež lauka PlatformName vērtību
     */
    public String getPlatformName() {
        return PlatformName;
    }

    /*
     Atgriež lauka SolutionName vērtību
     */
    public String getSolutionName() {
        return SolutionName;
    }

    /*
     Atgriež lauka NeededTools vērtību
     */
    public String getNeededTools() {
        return NeededTools;
    }

    /*
     Atgriež lauka SetupNotes vērtību
     */
    public String getSetupNotes() {
        return SetupNotes;
    }

    /*
     Atgriež lauka LocationsOfSolutions vērtību
     */
    public String getLocationsOfSolutions() {
        return LocationsOfSolutions;
    }

    /*
     Metode, kas aizpilda atribūtus
     */
    public void fillAttributes(String pPlatformName, String pSolutionName, String pNeededTools, String pSetupNotes, String pLocationsOfSolutions) {
        this.PlatformName = pPlatformName;
        this.SolutionName = pSolutionName;
        this.NeededTools = pNeededTools;
        this.SetupNotes = pSetupNotes;
        this.LocationsOfSolutions = pLocationsOfSolutions;
    }

}
