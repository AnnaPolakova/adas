package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/*
 Klase, kas pilnībā realizē PIAM modeļa loģiku.
 */
public class PIAM {

    /*
     Modeļa nosaukums
     */
    private String ModelName;

    /*
     Modeļa apraksts
     */
    private String ModelDescription;

    /*
     Modeļa struktūra - nepārtrauktās integrācijas serveris: vieta, no kurienes tiks pārvaldītas visas darbības.
     */
    private ContinuousIntegrationServer IntegrationServer;

    /*
     Modeļa notikumi (PIAMEvent).
     */
    private ArrayList<PIAMEvent> Events;

    /*
     PIAM modeļa darbības ar atribūtiem.
     */
    private ArrayList<SingleAction> PiamActions;

    /*
     PIAM modeļa konstruktors.
     */
    public PIAM(String pModelName, String pModelDescription) {
        this.ModelName = pModelName;
        this.ModelDescription = pModelDescription;

        //izveido tuksās struktūras
        IntegrationServer = new ContinuousIntegrationServer();
        Events = new ArrayList();
        PiamActions = new ArrayList();
    }

    /*
     Metode, kas pievieno notikumu
     */
    public void addEvent(PIAMEvent pEvent) {
        Events.add(pEvent);
    }

    /*
     Metode, kas aizpilda atribūtus nepārtrauktam integrācijas serverim
     */
    public void fillAttributesForContinuousIntegrationServer(String pPlatformName, String pSolutionName, String pNeededTools, String pSetupNotes, String pLocationsOfSolutions) {
        IntegrationServer.fillAttributes(pPlatformName, pSolutionName, pNeededTools, pSetupNotes, pLocationsOfSolutions);
    }

    /*
     Metode, kas atgriež darbību struktūru ar atribūtiem
     */
    public ArrayList<SingleAction> getPiamActions() {
	//Sākumā pārmeklē visas notikumus (Events) un katrā notikumā katru plūsmu. Sastāda darbību sarakstu bez dublikātiem.
        //pagaidu struktūra, kas glabās darbību sarakstu. Struktūra neļauj pievienot dublikātus, kas arī vajadzīgs šajā metodē.
        //HashSet ir datu struktūra JAVA valodā. Ja programma tiks izstrādāta izmantojot citu programmēšanas valodu, tad būs jāizmanto analoģiska struktūra vai arī jāizveido sava.
        HashSet<Actions> temp_action_list = new HashSet();
        //pārmeklē visus notikumus
        for (int i = 0; i <= Events.size(); i++) {
            //pārmeklē notikuma plūsmas
            for (int j = 0; j <= Events.get(i).getEventFlows().size(); j++) {
                //iegūst darbības no konkrētas plūsmas
                ArrayList<Actions> single_flow_action = Events.get(i).getEventFlows().get(j).getPiamActions();
                //katru darbību ievieto pagaidu sarakstā
                for (int k = 0; k <= single_flow_action.size(); k++) {
                    temp_action_list.add(single_flow_action.get(k));
                }
            }
        }
        //katrai darbībai, kas ir ielikta pagaidu sarakstā izveido objektu no klases SingleAction un ieliek to PiamActions struktūrā
        for (Iterator it = temp_action_list.iterator(); it.hasNext();) {
            Actions single_object_foraction = (Actions) it.next();
            PiamActions.add(new SingleAction(single_object_foraction));
        }

        //visbeidzot atgriež darbību struktūru ar atribūtiem
        return PiamActions;
    }

    /*
     Metode, kas atgriež modeļa nosaukumu
     */
    public String getModelName() {
        return ModelName;
    }

    /*
     Metode, kas atgriež modeļa aprakstu
     */
    public String getModelDescription() {
        return ModelDescription;
    }

    /*
     Metode, kas atgriež modeļa notikumus
     */
    public ArrayList<PIAMEvent> getEvents() {
        return Events;
    }

    /*
     Metode, kas atgriež nepārtrauktās integrācijas serveri
     */
    public ContinuousIntegrationServer getIntegrationServer() {
        return IntegrationServer;
    }
}
