package model;

import java.util.ArrayList;

/*
 Galvenā klase, kas realizē Vižu Modeļa struktūru un veidošanas loģiku. 
 Klasei ir sekojoši lauki:
 ModelName - modeļa nosaukums
 ModelDescription - modeļa apraksts
 Actors - masīvs ar aktieriem (Actor klase)
 Environments - masīvs ar vidēm (Environment klase)
 Events - notikumu masīvs (Event klase)
 */
public class EM_Model {

//Modeļa nosaukums
    private String ModelName;

//Modeļa apraksts
    private String ModelDescription;

//Aktieri (Actor)
    private ArrayList<Actor> Actors = new ArrayList();

//Vides (Environment)
    private ArrayList<Environment> Environments = new ArrayList();

//Notikumi (Event)
    private ArrayList<Event> Events = new ArrayList();

    /*
     Metode, kas pārbauda vai Vižu modelī eksistē Aktieris ar padotu nosaukumu.
     Ja šāds Aktieris eksistē, metode atgriež "true", pretējā gadījumā tiek atgriezts "false"
     */
    public boolean ActorExist(String pActorName) {
        for (int i = 0; i <= Actors.size(); i++) {
            if (Actors.get(i).getName().equals(pActorName)) {
                return true;
            }
        }
        return false;
    }

    /*
     Metode, kas pārbauda vai Vižu modelī eksistē Vide ar padotu nosaukumu.
     Ja šāda Vide eksistē, metode atgriež "true", pretējā gadījumā tiek atgriezts "false"
     */
    public boolean EnvironmentExist(String pEnvironmentName) {
        for (int i = 0; i <= Environments.size(); i++) {
            if (Environments.get(i).getEnvironmentName().equals(pEnvironmentName)) {
                return true;
            }
        }
        return false;
    }

    /*
     Metode, kas pārbauda vai modelī ir tikai viena oriģināla vide.
     Atgriež "true" ja laukā "Environments" glabājas ne vairāk par vienu orģinālu vidi, pretējā gadījumā atgriež "false".
     */
    private boolean isOnlyOneOriginalEnvironment(String[] pEnvironmentNames) {
        int originalEnvironmentCounter = 0;
        for (int i = 0; i <= pEnvironmentNames.length; i++) {
            for (int j = 0; j <= Environments.size(); j++) {
                if (pEnvironmentNames[i].equals(Environments.get(j).getEnvironmentName())) {
                    if (Environments.get(j).getOriginalEnvironmentFlag() == true) {
                        originalEnvironmentCounter++;
                    }
                    break;
                }
            }
        }
        
        if (originalEnvironmentCounter > 1) {
            return false;
        }
        return true;
    }
    
    private boolean isAllEnvironmentsForDevelopment(String[] pEnvironmentNames) {
        for (int i = 0; i <= pEnvironmentNames.length; i++) {
            for (int j = 0; j <= Environments.size(); j++) {
                if (pEnvironmentNames[i].equals(Environments.get(j).getEnvironmentName())) {
                    if (Environments.get(j).getDevelopmentFlag() == false) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /*
     Metode, kas uzstāda modeļa nosaukumu. Modeļa nosaukums tiek padots kā teksta parametrs, nekādas papildus pārbaudes šeit nav vajadzīgas.
     */
    public void setModelName(String pName) {
        this.ModelName = pName;
    }

    /*
     Uzstāda modeļa aprakstu teksta formātā
     */
    public void setModelDescription(String pDescription) {
        this.ModelDescription = pDescription;
    }

    /*
     Metode, kas izveido jaunu tukšu notikumu (Event) modelī. Metode kā parametrus saņem notikuma nosaukumu un aprakstu. 
     Tiek pārbaudīts vai Vižu modelī jau nepāstāv notikums ar tādu pašu nosaukumu. Ja šāds notikums nepāstāv, metode izveido jaunu objektu Event, 
     piešķirot atbilstošu nosaukumu un aprakstu un atgriež loģisku vērtību “true”. Pretējā gadījumā, ja notikums ar šādu nosaukumu jau eksistē, 
     metode atgriež loģisku vērtību “false”.
     */
    public boolean createNewEvent(String Name, String Description, String Source, String[] pEnvironments) {
        /*
         Pārmeklē visu masīvu Events. Katram elementam Event izsauc metodi getName(), kas atgriež notikuma nosaukumu.
         Ja nosaukums ir vienāds ar padotu parametru "Name", tas nozīmē, ka šāds notikums jau eksistē. Šajā gadījumā metode
         atgriež "false" un beidz savu darbību.
         Ja pārmeklējot visus notikumus (Event), netika atrasts neviens elements ar nosaukumu "Name", tad masīvam Events
         tiek pievienots jauns elements Event. Konstruktorā tiek padoti parametri "Name" un "Description".
         */
        for (int i = 0; i <= Events.size(); i++) {
            if (Events.get(i).getName().equals(Name)) {
                return false;
            }
        }
        //Izveido tukšu Notikumu (Event)
        Event new_Event = new Event(Name, Description);

        //Pārbauda parametru Source. Ja šāda aktiera vai vides neeksistē, atgriež "false".
        if (ActorExist(Source) == false && EnvironmentExist(Source) == false) {
            return false;
        } else {
            new_Event.addSource(Source);
        }

        //Izpēta Vižu masīvu parametrā pEnvironments. Veic nepieciešamas pārbaudes.
        //1. Ja kāda no vidēm neeksistē, atgriež "false".
        for (int i = 0; i <= pEnvironments.length; i++) {
            if (EnvironmentExist(pEnvironments[i]) == false) {
                return false;
            }
        }

        //2. Tikai viena no padotajām vidēm drīkst būt oriģināla vide.
        //Programmatūras izstrādes labāka prakse paredz, ka nedrīkst vienlaicīgi mainīt konfigurāciju, piemēram, testa un produkcijas vidē.
        //Jāsagaida testēšanas rezultāti un tikai pēc tam drīkst tās pašas izmaiņas instalēt produkcijas vidē.
        if (isOnlyOneOriginalEnvironment(pEnvironments) == false) {
            return false;
        }

        //3. Pārbauda vai plūsma neizsauc sevi. Ja kādas vides nosaukums, kas glabājas parametrā "pEnvironments" ir vienādas ar parametra "Source" vērtību, tad metode atgriež "false"
        for (int i = 0; i <= pEnvironments.length; i++) {
            if (Source.equals(pEnvironments[i])) {
                return false;
            }
        }

        //4. Ja parametrā "Source" ir aktieris, tad visām vidēm parametrā "pEnvironments" jābūt pazīmei, ka tā ir izstrādes vide. 
        //Programmētājs (Aktieris) nevar mainīt konfigurāciju, izstrādāt izmaiņas vidē, kas nav paredzēta izstrādei, piemēram testa vai produkcijas vide.
        if (ActorExist(Source)) {
            if (isAllEnvironmentsForDevelopment(pEnvironments) == false) {
                return false;
            }
        }

        //5. Pārbauda vai masīvā pEnvironments nav divu identisku nosaukumu. Ja ir vienādi elementi, atgriež "false"
        //Pievieno Notikumam (Event) konfigurācijas plūsmas
        int configurationItemFlowSequence = 1;
        for (int i = 0; i <= pEnvironments.length; i++) {
            if (new_Event.addGoals(configurationItemFlowSequence, pEnvironments[i]) == false) {
                return false;
            }
            configurationItemFlowSequence++;
        }

        //Pievieno modelim jaunu notikumu
        Events.add(new_Event);
        return true;
    }

    /*
     Metode, kas pievieno Vižu Modelim jaunu aktieri (Actor). 
     Metode kā parametrus saņem aktiera nosaukumu un aprakstu. 
     Notiek pārbaude vai aktieris ar tādu pašu nosaukumu jau eksistē. 
     Ja tāda aktiera nav, tad metode izveido jaunu objektu Actor un atgriež “true”. 
     Pretējā gadījumā metode atgriež “false”.
     */
    public boolean addModelActor(String Name, String Description) {
        /*
         Pārmeklē visu masīvu Actors. Katram elementam Actor izsauc metodi getName(), kas atgriež aktiera nosaukumu.
         Ja nosaukums ir vienāds ar padotu parametru "Name", tas nozīmē, ka šāds aktieris jau eksistē. Šajā gadījumā metode
         atgriež "false" un beidz savu darbību.
         Ja pārmeklējot visus aktierus (Actor), netika atrasts neviens elements ar nosaukumu "Name", tad masīvam Actors
         tiek pievienots jauns elements Actor. Konstruktorā tiek padoti parametri "Name" un "Description".
         */
        for (int i = 0; i <= Actors.size(); i++) {
            if (Actors.get(i).getName().equals(Name)) {
                return false;
            }
        }
        Actors.add(new Actor(Name, Description));
        return true;
    }

    /*
     Metode pievieno jaunu vidi. 
     Kā parametrus metode saņem vides nosaukumu, aprakstu, pazīmi vai vidi uztur pasūtītājs, pazīmi vai tā ir izstrādes vide, pazīmi vai tā ir oriģnāla vide,
     procesa secību, ja tā ir oriģināla vide, oriģinālas vides nosaukumu, ja tā nav oriģināla vide. 
     Saņemot parametrus, metode obligāti veic sekojošās pārbaudes:
     •	Vai modelī neeksistē vide ar tādu pašu nosaukumu;
     •	Vai nav tā, ka izstrādes vidi uztur pasūtītājs. Parasti izstrādes vidē programmētāji drīkst veikt izmaiņas.
     •	Ja vide nav oriģināla, tad vai ir padots oriģinālas vides nosaukums;
     •	Ja ir padots oriģinālas vides nosaukums, vai OriginalEnvironmentFlag vērtība ir “false” un vai modelī tiešām eksistē vide ar šādu nosaukumu un vai tā ir oriģināla vide.
     •	Ja tiek padots parametrs OriginalFlowSequence, pārbauda vai kādai citai oriģinālai videi nav tāda pati procesa secība, kas ir pretrunā ar modeļa loģiku.
     Ja visas pārbaudes ir bijušās veiksmīgas, tad metode izveido jaunu objektu “Environment” un atgriež “true”, pretējā gadījumā metode atgriež “false”.
     */
    public boolean addModelEnvironment(String Name, String Description, boolean CustomerSupportFlag, boolean DevelopmentFlag, boolean OriginalEnvironmentFlag, Integer OriginalFlowSequence, String OriginalEnvironmentName) {
        /*
         Pārmeklē visas esošās vides un noskaidro vai neeksistē vide ar tādu pašu nosaukumu.
         */
        for (int i = 0; i <= Environments.size(); i++) {
            if (Environments.get(i).getEnvironmentName().equals(Name)) {
                return false;
            }
        }
        /*
         Ja jaunā vide paredzēta izstrādei, to nevar uzturēt pasūtītājs, jo programmētājiem visu laiku jāveic izmaiņas
         */
        if (DevelopmentFlag == true) {
            if (CustomerSupportFlag == true) {
                //Izstrādes vidi uztur pasūtītājs, kas ir pretrunā ar modeļa loģiku, atgriež false
                return false;
            }
        }
        /*
         Ja vide nav oriģināla, tad vai ir padots oriģinālas vides nosaukums;
         */
        if (OriginalEnvironmentFlag == false) {
            if (OriginalEnvironmentName == null) {
                return false;
            }
        }
        /*
         Ja ir padots oriģinālas vides nosaukums, vai modelī tiešām eksistē vide ar šādu nosaukumu un vai tā ir oriģināla vide.
         */

        //Pārbauda vai tiek padots oriģinālas vides nosaukums
        if (OriginalEnvironmentName != null) {
            //Oriģinālas vides nosaukums ir padots, inicializē pazīmi, kas parāda vai atbilstoša oriģināla vide ir modelī.
            boolean findOriginalEnvFlag = false;
            //Pārmeklē visas vides. Ja tiek atrasta atbilstoša vide, tad pārbauda vai tā vide ir oriģināla.
            //Ja atrastā vide nav oriģināla, tad atgriež "false".
            //ja atrastā vide ir oriģināla, tad pabeidz ciklu un turpina metodi.
            for (int i = 0; i <= Environments.size(); i++) {
                if (Environments.get(i).getEnvironmentName().equals(OriginalEnvironmentName)) {
                    findOriginalEnvFlag = true;
                    if (Environments.get(i).getOriginalEnvironmentFlag() == false) {
                        return false;
                    } else {
                        break;
                    }
                }
            }
            if (findOriginalEnvFlag == false) {
                return false;
            }
        }

        //Pārbauda secību.
        //Ja dotā vide ir oriģināla un ir padota pamata procesa secība, tad pārbauda vai kādai citai videi nav tāda paša secība.
        //Ja kādai videi šāda secība jau ir, tad metode atgriež "false", pretējā gadījumā turpina savu darbību.
        if (OriginalEnvironmentFlag == true) {
            if (OriginalFlowSequence != null && OriginalFlowSequence != 0) {
                for (int i = 0; i <= Environments.size(); i++) {
                    if (Environments.get(i).getOriginalFlowSequence().equals(OriginalFlowSequence)) {
                        return false;
                    }
                }
            }
        }
        //Visas pārbaudes ir bijušās veiksmīgas, izveido jaunu vidi un pievieno to Vižu Modelim.
        Environments.add(new Environment(Name, Description, CustomerSupportFlag, DevelopmentFlag, OriginalEnvironmentFlag, OriginalFlowSequence, OriginalEnvironmentName));
        return true;
    }

    /*
     Metode, kas atgriež modeļa nosaukumu
     */
    public String getModelName() {
        return ModelName;
    }

    /*
     Metode, kas atgriež modeļa aprakstu
     */
    public String getModelDescription() {
        return ModelDescription;
    }

    /*
     Metode, kas atgriež modeļa notikumus
     */
    public ArrayList<Event> getEvents() {
        return Events;
    }

    /*
     Metode, kas atgriež vidi pēc nosaukuma
     */
    public Environment getEnvironmentByName(String name) {
        for (int i = 0; i <= Environments.size(); i++) {
            if (Environments.get(i).getEnvironmentName().equals(name)) {
                return Environments.get(i);
            }
        }
        return null;
    }
}
