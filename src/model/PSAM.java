package model;

import java.util.ArrayList;

/*
 Klase, kas pilnībā realizē PSAM modeļa loģiku.
 */
public class PSAM {

    /*
     Modeļa nosaukums
     */
    private String ModelName;

    /*
     Modeļa apraksts
     */
    private String ModelDescription;

    /*
     Modeļa struktūra - nepārtrauktās integrācijas serveris: vieta, no kurienes tiks pārvaldītas visas darbības.
     */
    private ContinuousIntegrationServer IntegrationServer;

    /*
     PSAM modeļa darbības ar atribūtiem.
     */
    private ArrayList<SingleAction> PsamActions;

    /*
     PSAM modeļa konstruktors.
     */
    public PSAM(String pModelName, String pModelDescription, ContinuousIntegrationServer pIntegrationServer, ArrayList<SingleAction> pPsamActions) {
        this.ModelName = pModelName;
        this.ModelDescription = pModelDescription;
        this.IntegrationServer = pIntegrationServer;
        this.PsamActions = pPsamActions;
    }

    /*
     Metode, kas atgriež darbību struktūru ar atribūtiem
     */
    public ArrayList<SingleAction> getPsamActions() {
        return PsamActions;
    }
    /*
     Metode, kas atgriež modeļa nosaukumu
     */

    public String getModelName() {
        return ModelName;
    }

    /*
     Metode, kas atgriež modeļa aprakstu
     */
    public String getModelDescription() {
        return ModelDescription;
    }

    /*
     Metode, kas atgriež nepārtrauktās integrācijas serveri
     */
    public ContinuousIntegrationServer getIntegrationServer() {
        return IntegrationServer;
    }
}
