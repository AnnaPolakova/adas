package model;

/*
Klase, kas realizē Vižu Modeļa "Actor" elementu.
Par Aktieri var būt programmētājs, kas veic izmaiņas izstrādes vidē.
*/
public class Actor{

/*Aktiera unikāls nosaukums
Te var būt gan programmētāja vārds, gan funkcija projektā, gan amats.
Piemēri: Jānis Bērziņš, Vecākais programmētājs, Uzturēšanas tehniskais speciālists.
*/
private String Name;

/*
Aktiera apraksts teksta formātā, detalizētāks skaidrojums.
Piemērs: Vecākais programmētājs - speciālists, kas atbild par izmaiņu pieprasījumu ieviešanu projektā.
*/
private String Description;
	
/*
Klases konstruktors. Kā parametrus saņem nosaukumu un aprakstu un izveido objektu, piešķirot konkrētas vērtības
klases laukiem Name un Description.
*/
public Actor(String pName, String pDescription){
	this.Name = pName;
	this.Description = pDescription;
	}

/*
Atgriež Name lauka vērtību
*/	
public String getName(){
	return Name;
	}
	
/*
Atgriež Description lauka vērtību
*/
public String getDescription(){
	return Description;
	}
}