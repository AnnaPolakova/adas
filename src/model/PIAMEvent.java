package model;

import java.util.ArrayList;

/*
 Klase, kas realizē izmaiņu pārnešanas notikumu PIAM modeļa kontekstā.
 */
public class PIAMEvent {

    /*
     Notikuma nosaukums.
     */
    private String EventName;

    /*
     Notikuma plūsmas ar darbībām.
     */
    private ArrayList<PIAMFlow> EventFlows;

    /*
     Lauks kas parāda vai šajā notikumā ir bijusi programmatūras kompilācija kādā no plūsmām.
     Pec noklusējuma, vērtība ir "false", taču ja notikumā tiek pievienota kaut viena plūsma, kurā tika būvēts produkts no izejas koda, vērtība paliek "true".
     Šīs lauks ir vajadzīgs transformācijas algoritmam no EM uz PIAM modeli.
     */
    private boolean compilationFlag = false;

    /*
     Notikuma konstruktors.
     */
    public PIAMEvent(String pEventName) {
        this.EventName = pEventName;
        //izveido tukšu masīvu ar plūsmām.
        EventFlows = new ArrayList();
    }

    /*
     Metode, kas pievieno gatavu izmaiņu plūsmu.
     */
    public void addEventFlow(PIAMFlow pFlow) {
        EventFlows.add(pFlow);
    }

    /*
     Metode, kas atgriež visas plūsmas
     */
    public ArrayList<PIAMFlow> getEventFlows() {
        return EventFlows;
    }

//Uzstāda kompilācijas pazīmi	
    public void setCompilationFlag(boolean pFlag) {
        this.compilationFlag = pFlag;
    }

//Atgriež kompilācijas pazīmi
    public boolean getCompilationFlag() {
        return compilationFlag;
    }

//Agriež notikuma nosaukumu
    public String getEventName() {
        return EventName;
    }

}
