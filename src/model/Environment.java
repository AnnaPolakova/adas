package model;

/*
 Klase, kas realizē Vižu Modeļa Environment elementu.
 Environment (Vide) ir infrastruktūras kopa, kas paredzēta konkrētam procesam programmatūras izstrādes projektā.
 Piemēram, programmatūras izstrādei nepieciešama sekojoša infrastruktūra: datubāzes serveris, aplikāciju serveris.
 Šo infrastruktūru, ko izmantos programmatruras izstrādei, var nosaukt par DEV (Development Environment).
 Analoģisku infrastuktūru, kas ir paredzēta testēšanas procesam, varētu nosaukt par TEST.
 Šajā Vižu Modelī Environment (Vide) elements tiek modelēts tikai no izmaiņu pārnešanas aspekta. Nekādas detaļas par
 infrastruktūru, tehnoloģiju, platformu nav zināmas. No izmaiņu pārnešanas viedokļa ir svarīgi un šajā modelī atspoguļoti sekojoši atribūti:
 - Vides unikāls nosaukums, piemēram, DEV, TEST, QA, PROD.
 - Vides apraksts, kas sniedz lielāku priekšstatu par Vides nozīmi un lomu projektā.
 - Pazīme vai Vidi uztur pasūtītājs vai izpildītājs.
 - Pazīme vai vide ir paredzēta izmaiņu izstrādei (programmētāji drīkst manuāli veikt izmaiņas kodā un konfigurācijā).
 - Pazīme vai vide tiek izmantota reālajā projekta procesā (izstrāde, testēšana, kvalitātes akcepttestēšana, ekspluatācija) vai tikai būvējumu testēšanas vajadzībām.
 - Procesa secība.
 Klasiskais variants:
 1 - DEV vide
 2 - TEST vide
 3 - QA vide
 4 - PROD vide
 - Oriģinālas vides nosaukums. Gadījumā, ja dotā vide netiek izmantota reālajā procesā, bet tikai būvējumu testēšanai, tad norāda kuras oriģinālas vides kopija tā ir.
 */
public class Environment {

//Vides unikāls nosaukums
    private String Name;

//Vides apraksts brīvā teksta formātā
    private String Description;

//Pazīme, kas nosaka, vai vidi uztur pasūtītājs
    private boolean CustomerSupportFlag;

//Pazīme, kas nosaka vai vide ir paredzēta izstrādes darbiem - programmētāji drīkst manuāli veikt izmaiņas kodā un konfigurācijā
    private boolean DevelopmentFlag;

//Pazīme, kas nosaka vai vide tiek izmantota reālajā procesā vai tikai būvējumu testēšanai vai speciālas versijas izstrādei
    private boolean OriginalEnvironmentFlag;

    /*Procesa secība.
     Klasiskais piemērs:
     1 - DEV vide
     2 - TEST vide
     3 - QA vide
     4 - PROD vide
     Ja vide nav oriģināla, tad vērtība ir 0.
     */
    private Integer OriginalFlowSequence;

    /*
     Oriģinālas vides nosaukums gadījumā ja šī konkrēta vide ir kopija kādai oriģinālai videi un paredzēta būvējumu testēšanai vai speciālas versijas izstrādei.
     Ja tā ir oriģināla vide, tad šī lauka vērtība ir null.
     */
    private String OriginalEnvironmentName;

    /*
     Klases konstruktors. Kā parametrus saņem:
     - Vides nosaukumu,
     - Vides aprakstu,
     - Pazīmi vai vidi uztur pasūtītājs,
     - Pazīmi vai tā ir izstrādes vide,
     - Pazīme vai tā ir oriģināla vide (tiek izmantota kāda no procesiem nevis tikai būvējumu testēšanai vai speciālas versijas sagatavošanai),
     - Procesa secība gadījumam ja tā ir oriģināla vide,
     - Oriģinālas vides nosaukums.
     Piešķir klases laukiem atbilstošās vērtības.
     */
    public Environment(String Name, String Description, boolean CustomerSupportFlag, boolean DevelopmentFlag, boolean OriginalEnvironmentFlag, Integer OriginalFlowSequence, String OriginalEnvironmentName) {
        this.Name = Name;
        this.Description = Description;
        this.CustomerSupportFlag = CustomerSupportFlag;
        this.DevelopmentFlag = DevelopmentFlag;
        this.OriginalEnvironmentFlag = OriginalEnvironmentFlag;
        this.OriginalFlowSequence = OriginalFlowSequence;
        this.OriginalEnvironmentName = OriginalEnvironmentName;
    }

    /*
     Atgriež Vides nosaukumu
     */
    public String getEnvironmentName() {
        return Name;
    }

    /*
     Atgriež Vides aprakstu
     */
    public String getEnvironmentDescription() {
        return Description;
    }

    /*
     Atgriež pazīmi vai vide ir oriģināla
     */
    public boolean getOriginalEnvironmentFlag() {
        return OriginalEnvironmentFlag;
    }

    /*
     Atgriež pamata procesa secību
     */
    public Integer getOriginalFlowSequence() {
        return OriginalFlowSequence;
    }

    /*
     Atgriež lauka DevelopmentFlag vērtību
     */
    public boolean getDevelopmentFlag() {
        return DevelopmentFlag;
    }

    /*
     Atgriež lauka CustomerSupportFlag vērtību
     */
    public boolean getCustomerSupportFlag() {
        return CustomerSupportFlag;
    }

    /*
     Atgriež lauka OriginalEnvironmentName vērtību
     */
    public String getOriginalEnvironmentName() {
        return OriginalEnvironmentName;
    }

}
