package model;

import java.util.HashMap;

/*
 Klase, kas realizē transformāciju no Vižu Modeļa (EM) uz PIAM (No Platformas Neatkarīgs Darbību Modelis)
 */
public class TransformationManager {

//No Platformas Neatkarīgs Darbību Modelis
    private PIAM PiamModel;

    /*
     Metode atgriež gatavu PIAM modeli.
     */
    public PIAM getPiamModel() {
        return PiamModel;
    }

    /*
     Metode, kas kā parametru saņem objektu EM_Model un veic transformāciju uz PIAM
     */
    public void makeTransformation(EM_Model pEnvironmentModel) {
	//Izveido jaunu PIAM modeli
        //Nosaukumu un aprakstu paņem no padotā EM modeļa
        PiamModel = new PIAM(pEnvironmentModel.getModelName(), pEnvironmentModel.getModelDescription());

        //pārmeklē visus notikumus EM_Model objektā
        Event[] allEventsFromEmModel = (Event[])pEnvironmentModel.getEvents().toArray();
        for (int i = 0; i <= allEventsFromEmModel.length; i++) {
            //Izveido notikumu PIAM modelī
            PIAMEvent current_piam_event = new PIAMEvent(allEventsFromEmModel[i].getName());
		//Iegūst izmaiņu plūsmas no notikuma EM_Model objektā
            //HashMap ir datu struktūra javā, kas glabā vienveidīgus elementus formātā: key, elements value. Pec key (atslēgas) var dabūt elementa vērtību
            //Ja programma tiks izstrādāta citā programmēšanas valodā, tad jāizmanto analoģiska struktūra vai jārealizē sava
            HashMap<Integer, String> goals_from_em_model = allEventsFromEmModel[i].getGoals();
            //Katram plūsmas mērķim (Goal) EM modeļa notikumā izveido plūsmu PIAM modelī
            for (int j = 0; j <= goals_from_em_model.size(); j++) {
                int current_flow_number = j + 1;
                String current_goal = goals_from_em_model.get(current_flow_number);
                PIAMFlow current_piam_flow = new PIAMFlow(current_flow_number);
				//Pielieto transformācijas likumus, kas nosaka kādas darbības vajadzīgas, lai realizētu šo izmaiņu plūsmu

				//1
				/*
                 Ja izmaiņas netiek pārnestas no kādas vides, bet tās veido programmētājs, tad jābūt “DEVELOPMENT” darbībai, 
                 kas šī darba kontekstā nozīmē likumus, kas reglamentē produkta izmaiņu veikšanu. Izstādes beigās rezultāti obligāti jāsaglabā. 
                 Tiek pielietota darbība “COMMIT_CHANGES”, kas nozīmē, ka visām produktu vienībām jābūt pakļautam versiju kntrolei.
                 */
                if (pEnvironmentModel.ActorExist(allEventsFromEmModel[i].getSource()) == true) {
                    current_piam_flow.addPiamAction(Actions.DEVELOPMENT);
                    current_piam_flow.addPiamAction(Actions.COMMIT_CHANGES);
                }

				//2
				/*
                 Ja nepieciešams pārnest konfigurāciju no vides kopijas uz vidi, kuru uztur pasūtītājs un konkrēta notikuma ietvaros tā ir pirmā plūsma, 
                 tad nepieciešams uzkompilēt produktu, noformēt pasūtītājam uzkompilēta produkta piegādi ar visu pavadošu dokumentāciju un sagaidīt apstiprinājumu, 
                 ka pasūtītājs veiksmīgi ir uzinstalējis šo produktu, lai tālāk varētu aktivizēt citu plūsmu, kas, piemēram, uzliek tādu pašu konfigurāciju atbilstošas vides kopijā.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == false
                        && current_flow_number == 1
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == true) {
                    current_piam_flow.addPiamAction(Actions.BUILD_PRODUCT);
                    current_piam_event.setCompilationFlag(true);
                    current_piam_flow.addPiamAction(Actions.DELIVERY_PRODUCT);
                    current_piam_flow.addPiamAction(Actions.ENV_UPDATE_NOTIFICATION);
                }

				//3
				/*
                 Ja nepieciešams pārnest konfigurāciju no vides kopijas uz citu vidi, kuru neuztur pasūtītājs, tad un tā ir pirmā plūsma notikumā, 
                 tad nepieciešams uzkompilēt produktu un uzinstalēt to atbilstošā vidē.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == false
                        && current_flow_number == 1
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == false) {
                    current_piam_flow.addPiamAction(Actions.BUILD_PRODUCT);
                    current_piam_event.setCompilationFlag(true);
                    current_piam_flow.addPiamAction(Actions.INSTALL_PRODUCT);
                }

				//4
				/*
                 Ja nepieciešams pārnest konfigurāciju no vides kopijas uz vidi, kuru uztur pasūtītājs un tā nav pirmā plūsma, tad nepieciešams izveidot produkta piegādi, 
                 par pamatu ņemot kompilāciju no iepriekšējām plūsmām, lai mazinātu riskus, un sagaidīt apstiprinājumu, ka pasūtītājs ir uzinstalējis produktu atbilstošā vidē, 
                 lai tālāk varētu aktivizēt citu plūsmu šajā pašā notikumā.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == false
                        && current_flow_number != 1
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == true) {
                    current_piam_flow.addPiamAction(Actions.DELIVERY_PRODUCT);
                    current_piam_flow.addPiamAction(Actions.ENV_UPDATE_NOTIFICATION);
                }

				//5
				/*
                 Ja nepieciešams pārnest konfigurāciju no vides kopijas uz kādu citu vidi, ko neuztur pasūtītājs un tā nav pirmā plūsma notikumā, 
                 tad nepieciešams tikai uzinstalēt produktu, par pamatu ņemot kompilāciju no iepriekšējām plūsmām šajā pašā notikumā.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == false
                        && current_flow_number != 1
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == false) {
                    current_piam_flow.addPiamAction(Actions.DELIVERY_PRODUCT);
                    current_piam_flow.addPiamAction(Actions.ENV_UPDATE_NOTIFICATION);
                }

				//6
				/*
                 Ja ir nepieciešams pārnest konfigurāciju starp divām oriģinālām vidēm un abas vides uztur pasūtītājs, tad šajā gadījumā pārnešanu pilnībā veic pasūtītājs 
                 un par pamatu viņš ņem gatavu piegādi, kas tika instalēta sākotnējā vidē. Piemēram, ekspluatācijas vidē jāliek tieši tādas pašas izmaiņas kas tika liktas testa vidē, 
                 pretējā gadījumā pastāv nopietni riski vides pieejamībai.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && (pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getCustomerSupportFlag() == true
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == true) == true) {
                    current_piam_flow.addPiamAction(Actions.ENV_UPDATE_NOTIFICATION);
                }

				//7
				/*
                 Ja konfigurācija ir jāpārnes no oriģinālās vides, kuru neuztur pasūtītājs, uz citu vidi, kuru arī neuztur pasūtītājs un ja šī plūsma ir pirmā notikumā, 
                 tad nepieciešams sagatavot vides izejas kodu, uzkompilēt produktu un uzinstalēt to.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && (pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getCustomerSupportFlag() == false) == true
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == false
                        && current_flow_number == 1) {
                    current_piam_flow.addPiamAction(Actions.PREPARE_BASELINE);
                    current_piam_flow.addPiamAction(Actions.BUILD_PRODUCT);
                    current_piam_event.setCompilationFlag(true);
                    current_piam_flow.addPiamAction(Actions.INSTALL_PRODUCT);
                }

				//8
				/*
                 Ja konfigurācija ir jāpārnes no oriģinālās vides, kuru neuztur pasūtītājs, uz citu vidi, kuru arī neuztur pasūtītājs, ja tā nav pirmā plūsma notikumā 
                 un ja kādā no iepriekšējām plūsmām produkts jau tika kompilēts, tad nepieciešams tikai uzinstalēt to, par pamatu ņemot jau esošu būvējumu.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && (pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getCustomerSupportFlag() == false) == true
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == false
                        && current_flow_number != 1
                        && current_piam_event.getCompilationFlag() == true) {
                    current_piam_flow.addPiamAction(Actions.INSTALL_PRODUCT);
                }

				//9
				/*
                 Ja konfigurācija ir jāpārnes no oriģinālās vides, kuru neuztur pasūtītājs, uz citu vidi, kuru arī neuztur pasūtītājs, ja tā nav pirmā plūsma notikumā 
                 un ja nevienā no iepriekšējām plūsmām produkts nav kompilēts, tad nepieciešams sagatavot izejas kodu, uzkompilēt un uzinstalēt produktu.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && (pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getCustomerSupportFlag() == false) == true
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == false
                        && current_flow_number != 1
                        && current_piam_event.getCompilationFlag() == false) {
                    current_piam_flow.addPiamAction(Actions.BUILD_PRODUCT);
                    current_piam_event.setCompilationFlag(true);
                    current_piam_flow.addPiamAction(Actions.INSTALL_PRODUCT);
                }

				//10
				/*
                 Ja nepieciešams pārnest konfigurāciju no oriģinālas vides, kuru neuztur pasūtītājs, uz vidi, kuru uztur pasūtītājs, un ja tā ir pirmā plūsma notikumā, 
                 tad nepieciešams sagatavot vides izejas kodu, uzkompilēt produktu, sagatavot piegādi un sagaidīt apstiprinājumu, ka pasūtītājs produktu uzinstalēja, 
                 lai varētu aktivizēt citu notikumu ja tāds ir paredzēts.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && (pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getCustomerSupportFlag() == false) == true
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == true
                        && current_flow_number == 1) {
                    current_piam_flow.addPiamAction(Actions.PREPARE_BASELINE);
                    current_piam_flow.addPiamAction(Actions.BUILD_PRODUCT);
                    current_piam_event.setCompilationFlag(true);
                    current_piam_flow.addPiamAction(Actions.DELIVERY_PRODUCT);
                    current_piam_flow.addPiamAction(Actions.ENV_UPDATE_NOTIFICATION);
                }

				//11
				/*
                 Ja nepieciešams pārnest konfigurāciju no oriģinālas vides, kuru neuztur pasūtītājs, uz vidi, kuru uztur pasūtītājs, ja tā nav pirmā plūsma notikumā un 
                 kādā no iepriekšējām plūsmām eksistē kompilācijas darbība, tad par pamatu jāņem jau uzkompilētais produkts un tikai jāuztaisa tam piegāde un jāsagaida apstiprinājums, 
                 ka klients produktu ir uzinstalējis atbilstošajā vidē.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && (pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getCustomerSupportFlag() == false) == true
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == true
                        && current_flow_number != 1
                        && current_piam_event.getCompilationFlag() == true) {
                    current_piam_flow.addPiamAction(Actions.DELIVERY_PRODUCT);
                    current_piam_flow.addPiamAction(Actions.ENV_UPDATE_NOTIFICATION);
                }

				//12
				/*
                 Ja nepieciešams pārnest konfigurāciju no oriģinālas vides, kuru neuztur pasūtītājs, uz vidi, kuru uztur pasūtītājs, ja tā nav pirmā plūsma notikumā un nevienā no iepriekšējām 
                 plūsmām neeksistē kompilācijas darbība, tad nepieciešams sagatavot vides izejas kodu, uzkompilēt produktu, sagatavot piegādi un sagaidīt apstiprinājumu, ka pasūtītājs produktu uzinstalēja, 
                 lai varētu aktivizēt citu notikumu ja tāds ir paredzēts.
                 */
                if (pEnvironmentModel.EnvironmentExist(allEventsFromEmModel[i].getSource()) == true
                        && (pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getOriginalEnvironmentFlag() == true
                        && pEnvironmentModel.getEnvironmentByName(allEventsFromEmModel[i].getSource()).getCustomerSupportFlag() == false) == true
                        && pEnvironmentModel.getEnvironmentByName(current_goal).getCustomerSupportFlag() == true
                        && current_flow_number != 1
                        && current_piam_event.getCompilationFlag() == false) {
                    current_piam_flow.addPiamAction(Actions.PREPARE_BASELINE);
                    current_piam_flow.addPiamAction(Actions.BUILD_PRODUCT);
                    current_piam_event.setCompilationFlag(true);
                    current_piam_flow.addPiamAction(Actions.DELIVERY_PRODUCT);
                    current_piam_flow.addPiamAction(Actions.ENV_UPDATE_NOTIFICATION);
                }
                //pievieno plūsmu PIAM notikumam
                current_piam_event.addEventFlow(current_piam_flow);
            }
            //Pievieno notikumu PIAM modelī
            PiamModel.addEvent(current_piam_event);
        }
    }
}
