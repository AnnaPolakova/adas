package model;

import java.util.HashMap;

/*
 Klase kas realizē Vižu Modeļa elementu Event (Notikums).
 Notikums modelē programmatūras izmaiņu pārnešanu no Aktiera (Actor) vai Vides (Environment) uz vienu vai vairākām citām vidēm.
 Pārnesot programmatūras izmaiņas no vides A uz vidi B, pastāv risks, ka pārnešanas rezultātā
 vide B būs nepieejama. Tas var notikt, piemēram, ja instalācija ir bijusi neveiksmīga, datubāzē ir invalīdi objekti, nevar pacelt aplikāciju serveri utt.
 Lai mazinātu šādu risku, praksē pielieto vides kopijas. Izveido vidi B1, kas ir B vides pēc iespējas
 precizāka kopija. B1 vides nepieejamība neietekmē projekta procesus, piemēram, testēšanu un tā var kādu laiku
 nefunkcionēt. Tāpēc viena Notikuma (Event) ietvaros programmatūras izmaiņas sākumā pārnes uz vidi B1 un ja
 pārnešana ir bijusi veiksmīga, tad to pašu būvējumu uzinstalē arī B vidē. B vide šī risinājuma kontekstā ir
 oriģināla vide, jo tā tiek izmantota programmatūras izstrādes procesā, piemēram, testēšanā. 
 Oriģinālas vides pieejamība ir kritiska projektam.

 Klases Event lauki:
 - Name - unikāls nosaukums,
 - Description - apraksts teksta formātā,
 - AllChangesMoveFlag - pazīme, kas norāda vai tiek pārnestas visas izmaiņas vai tikai noteiktā daļa no tām,
 - Source - izmaiņu avots. Var būt gan Aktieris (Actor) gan Vide (Environment),
 - Goals - struktūra, kas glabā vižu sarakstu uz kurieni nonāk programmatūras izmaiņas. Katrai videi ir kārtas
 numurs, kas norāda kādā secībā tiek pārnestas izmaiņas šī konkrēta notikuma ietvaros.
 */
public class Event {

    /*
     Unikāls nosaukums.
     */
    private String Name;

    /*
     Notikuma (Event) apraksts teksta formātā
     */
    private String Description;

    /*
     Pazīme, kas nosaka vai šajā notikumā (Event) no vides A uz vidi B tiek pārnestas visas izmaiņas vai tikai noteiktas.
     */
    private boolean AllChangesMoveFlag;

    /*
     Aktiera (Actor) vai vides nosaukums, no kurienes tiek ņemtas izmaiņas konfigurācijas plūsmām
     */
    private String Source;

    /*
     Programmatūras izmaiņu mērķa vides. Viena Notikuma (Event) ietvaros konfigurācija var tikt pārnesta vairākās vidēs.
     Piemēram, galvenais uzdevums ir pārnest programmatūras izmaiņas no vides DEV uz vidi TEST.
     Lai nepakļautu riskam TEST vidi, būvējumu uzinstalē Pre_TEST vidē, kas ir TEST vides kopija. Ja būvējuma instalācija
     ir bijusi veiksmīga, to pašu būvējumu uzinstalē TEST vidē. TEST vidē šajā gadījumā ir oriģināla vide šī risinājuma kontekstā,
     jo tiek izmantota reālam testēšanas procesam. Savukārt Pre_TEST vide ir kopija, kas tiek izmantota, lai notestētu būvējumus, 
     kas ir paredzēti TEST videi.

     HasMap<Key, Element> - JAVA datu struktūra, kas satur elementus formātā: atslēga(Key):Elements(Element). Struktūra neļauj
     glabāt vienādus elementus, kas ir vajadzīgs šī risinājuma gadījumā. Notikums (Event) var glabāt sevī vairākas plūsmas, kas
     nozīmē, ka vienas un tās pašas programmatūras izmaiņas var tiks pārvietotas uz vairākām vidēm, taču plūsmas kārtas numuram
     jābūt unikālam. Ja programmatūra netiks izstrādāta uz JAVA, tad jāizvēlas analoģiskā struktūra no citas programmēšanas valodas
     vai jārealizē sava.
     */
    private HashMap<Integer, String> Goals;

    /*
     Klases konstruktors. Kā parametrus saņem nosaukumu un aprakstu un izveido objektu, piešķirot konkrētas vērtības
     klases laukiem Name un Description.
     */
    public Event(String pName, String pDescription) {
        this.Name = pName;
        this.Description = pDescription;
    }

    /*
     Atgriež Name lauka vērtību
     */
    public String getName() {
        return Name;
    }

    /*
     Atgriež Description lauka vērtību
     */
    public String getDescription() {
        return Description;
    }

    /*
     Atgriež Source lauka vērtību
     */
    public String getSource() {
        return Source;
    }

    /*
     Atgriež AllChangesMoveFlag lauka vērtību.
     AllChangesMoveFlag - pazīme, kas norāda vai no vides, kas glabājas laukā "Source" tiek pārnestas visas izmaiņas vai tikai noteiktās.
     */
    public boolean getAllChangesMoveFlag() {
        return AllChangesMoveFlag;
    }

    /*
     Atgriež visas konfigurācijas plūsmas laukā Goals
     */
    public HashMap<Integer, String> getGoals() {
        return Goals;
    }

    /*
     Metode, kas piešķir Notikumam izmaiņu avotu, kas ir vai nu Aktieris vai vide.
     */
    public void addSource(String pSourceName) {
        this.Source = pSourceName;
    }

    /*
     Metode, kas pievieno konfigurācijas plūsmu un atgriež loģisku vērtību "true", ja plūsmu izdevās veiksmīgi pievienot.
     */
    public boolean addGoals(Integer pFlowNumber, String pEnvironmentName) {
        try {
            Goals.put(pFlowNumber, pEnvironmentName);
        } catch (Exception Ex) {
            return false;
        }
        return true;
    }
}
