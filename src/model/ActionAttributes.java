package model;

/*
 Klase, kas realizē struktūru, kas glabā PIAM un PSAM modeļu risinājumu atribūtus.
 Failā Actions.java ir uzskaitītas visas abstraktas darbības. Gan PIAM gan PSAM modeļos
 katrai darbībai ir atribūti, kas ir aprakstīti šajā klasē.
 Atšķirība ir tāda, ka PIAM modelī atribūtu vērtības vēl ir tukšas, taču PSAM modelī
 vērtības tiek aizpildītas.
 */
public class ActionAttributes {

    /*
     Risinājuma identifikācijas numurs
     */
    private Integer SolutionID;

    /*
     Atsauce uz platformu
     */
    private Integer PlatformID;

    /*
     Risinājuma unikāls nosaukums
     */
    private String SolutionName;

    /*
     Vajadzīgu rīku uzskaitījums
     */
    private String NeededTools;

    /*
     Gatavu risinājumu atrašanas vietas
     */
    private String LocationsOfSolutions;

    /*
     Risinājuma apraksts brīvā formātā
     */
    private String Description;

    /*
     Metode, kas aizpilda atribūtus
     */
    public void fillAttributes(Integer pSolutionID, Integer pPlatformID, String pSolutionName, String pNeededTools, String pLocationsOfSolutions, String pDescription) {
        this.SolutionID = pSolutionID;
        this.PlatformID = pPlatformID;
        this.SolutionName = pSolutionName;
        this.NeededTools = pNeededTools;
        this.LocationsOfSolutions = pLocationsOfSolutions;
        this.Description = pDescription;
    }

    /*
     Atgriež SolutionID lauka vērtību
     */
    public Integer getSolutionID() {
        return SolutionID;
    }

    /*
     Atgriež PlatformID lauka vērtību
     */
    public Integer getPlatformID() {
        return PlatformID;
    }

    /*
     Atgriež SolutionName lauka vērtību
     */
    public String getSolutionName() {
        return SolutionName;
    }

    /*
     Atgriež NeededTools lauka vērtību
     */
    public String getNeededTools() {
        return NeededTools;
    }

    /*
     Atgriež LocationsOfSolutions lauka vērtību
     */
    public String getLocationsOfSolutions() {
        return LocationsOfSolutions;
    }

    /*
     Atgriež Description lauka vērtību
     */
    public String getDescription() {
        return Description;
    }
}
