package model;

/*
 Struktūra, kas glabā abstraktas darbības PIAM un PSAM modeļiem.
 Darbību apraksts:
 DEVELOPMENT:
 Izmaiņu izstrādes darbība ir iekļauta PIAM modelī, jo jābūt noteikumiem kā veikt izmaiņas programmatūrā. 
 Modelējot šo darbību ir iespējams nodefinēt, piemēram, noteiktas tehnoloģijas un noteikta projekta 
 konfigurācijas vienumu veidošanas principus, procedūru kā izmaiņas tiek saskaņotas, rīkus, kas veic izmaiņu pārvaldību, 
 reglamentējošos dokumentus, instrukcijas utt.
	
 COMMIT_CHANGES:
 Darbība, kas nosaka izstrādātu izmaiņu saglabāšanu centralizētā repozitorijā. Respektīvi, visas izstrādātas izmaiņas produktā 
 jāpakļauj versiju kontrolei, lai varētu vienmēr redzēt kad, kurš un kāpēc veica izmaiņas. Šī darbība modelē versiju 
 kontroli konkrētā projektā un likumus, kurus jāievēro izstrādātājiem, saglabājot izmaiņas versiju kontroles sistēmā. 
 Darbības atribūtā var būt uzskaitīti arī rīki, kas automātiski kontrolē vai tiek ievēroti nepieciešami likumi un principi.
	
 PREPARE_BASELINE:
 Šī darbība atkarība no versiju kontroles sistēmas reglamentē procedūru un tehnisku atbalstu izejas koda sagatavošanai konkrētai videi. 
 Vižu Modelis paredz, ka katrai oriģinālai videi ir sava bāzes līnija – produkta izejas koda stāvoklis, kas atbilst vides konfigurācijai. 
 Līdz ar to pirms jebkādas konfigurācijas pārnešanas no vienas vides uz otru, nepieciešams sākumā atjaunot izejas kodu atbilstošai videi. 
 Praksē šī darbība paredz izmaiņu pārnešanu starp diviem versiju kontroles sistēmas zariem, ko vēl sauc par saplūdināšanu (angļ: merge). 
 Šī darbība apraksta kā notiek konfigurācijas pārnešana starp diviem repozitorija zariem un sniedz aprakstu rīkiem, kas to atbalsta tehniski.
	
 BUILD_PRODUCT:
 Darbība, kas no atbilstoša izejas koda uztaisa izpildāmu failu (EXE). Kompilācijas un būvējuma rezultātā vajazdzētu izveidoties izpildamam vienumam, 
 kas būtu spējīgs uzinstalēt produktu konkrētajā vidē.
	
 INSTALL_PRODUCT:
 Darbība, kas uzinstalē uzbūvētu produktu noteiktā vidē.
	
 DELIVERY_PRODUCT:
 Darbība, kas sagatavo un nosūta pasūtītājam uzbūvētu produktu. Šī darbība ir nepieciešama, lai produktu varētu uzinstalēt vidē, ko uztur pasūtītājs 
 un kurai izstrādātāju komandai nav pieejas, piemēram, ekspluatācijas vidē (PROD). Darbība modelē procedūru pie kuras jāpieturas, sagatavojot produktu nosūtīšanai. 
 Piemēram, kopā ar produkta būvējumu jānosūta arī versijas aprakstu un instrukciju kā produktu uzinstalēt kādā vidē.
	
 ENV_UPDATE_NOTIFICATION:
 Darbība, kas nosūta izstrādātāju komandai notifikāciju par to, ka kāda no vidēm, ko uztur pasūtītājs, ir uzinstalēta jauna produkta versija. 
 Šajā momentā izstrādātāju komandai kā minimums vajadzētu piefiksēt šo faktu atbilstoši papildinot informāciju versiju kontroles sistēmā ka arī jāuzinstalē 
 tādu pašu produkta versiju visās vidēs, kas ir kopijas atbilstošai pasūtītāja videi. Šajā procesā obligāti ir jāatjauno atbilstošās vides izejas kodu.
 */
public enum Actions {

    DEVELOPMENT, COMMIT_CHANGES, PREPARE_BASELINE, BUILD_PRODUCT, INSTALL_PRODUCT, DELIVERY_PRODUCT, ENV_UPDATE_NOTIFICATION
}
