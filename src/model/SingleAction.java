package model;

/*
 Klase, kas realizē vienu darbību ar atribūtiem.
 */
public class SingleAction {

    /*
     Darbības unikāls nosaukums, kas tiek izvēlēts no struktūras "Actions".
     */
    private Actions PiamAbstractAction;

    /*
     Darbības atribūti. Atribūti ir tukši PIAM modelī un ir aizpildīti PSAM modelī.
     */
    private ActionAttributes Attributes;

    /*
     Darbības konstruktors.
     */
    public SingleAction(Actions pAction) {
        //izveido tukšu atribūtu struktūru
        Attributes = new ActionAttributes();

        //piešķir darbības nosaukumu no fiksētas struktūras
        this.PiamAbstractAction = pAction;
    }

    /*
     Metode, kas atgriež lauka PiamAbstractAction vērtību
     */
    public Actions getPiamAbstractAction() {
        return PiamAbstractAction;
    }

    /*
     Metode, kas atgriež atribūtu struktūru
     */
    public ActionAttributes getAttributes() {
        return Attributes;
    }

    /*
     Metode, kas aizpilda atribūtus
     */
    public void fillAttributes(Integer pSolutionID, Integer pPlatformID, String pSolutionName, String pNeededTools, String pLocationsOfSolutions, String pDescription) {
        Attributes.fillAttributes(pSolutionID, pPlatformID, pSolutionName, pNeededTools, pLocationsOfSolutions, pDescription);
    }

}
