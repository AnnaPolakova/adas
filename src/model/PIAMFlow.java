package model;

import java.util.ArrayList;

/*
 Klase, kas realizē atsevisķu programmatūras izmaiņu plūsmu no vienas vides uz otru PIAM modeļa kontekstā
 */
public class PIAMFlow {

    /*
     Plūsmas numurs
     */
    private Integer FlowNumber;

    /*
     Darbību masīvs
     */
    private ArrayList<Actions> PiamActions;

    /*
     Plūsmas konstruktors
     */
    public PIAMFlow(Integer pFlowNumber) {
        //Izveido tukšu masīvu, kurš ir gatavs uzņemt darbības
        PiamActions = new ArrayList();

        //Piešķir plūsmas numuru
        this.FlowNumber = pFlowNumber;
    }

    /*
     Darbības pievienošanas metode
     */
    public void addPiamAction(Actions pAction) {
        PiamActions.add(pAction);
    }

    /*
     Metode, kas atgriež darbības
     */
    public ArrayList<Actions> getPiamActions() {
        return PiamActions;
    }

}
